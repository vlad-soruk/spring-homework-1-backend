INSERT INTO customers(name, email, age) VALUES('Kim Rek', 'kimrek@gmail.com', 22);
INSERT INTO customers(name, email, age) VALUES('Sasha Perez', 'sashaperez@gmail.com', 34);
INSERT INTO customers(name, email, age) VALUES('Carl Ronal', 'carlronal@gmail.com', 57);
INSERT INTO customers(name, email, age) VALUES('Joe Bid', 'joebid@gmail.com', 20);
INSERT INTO customers(name, email, age) VALUES('Donald Tusk', 'donaldtusk@gmail.com', 67);

INSERT INTO accounts(balance, currency, number, customer_id) VALUES(0, 'UAH', '759e6f0d-6f20-43f6-87aa-7215df087fae', 1);
INSERT INTO accounts(balance, currency, number, customer_id) VALUES(0, 'GBP', 'ec010ac7-c77f-4af2-b4ce-5a7788e150cc', 2);
INSERT INTO accounts(balance, currency, number, customer_id) VALUES(0, 'CHF', '811a48b3-3b00-471d-b4d9-7a52861cbefc', 3);
INSERT INTO accounts(balance, currency, number, customer_id) VALUES(0, 'USD', '567a18fc-b599-4cae-aaf7-8d246a9dda9b', 4);
INSERT INTO accounts(balance, currency, number, customer_id) VALUES(0, 'EUR', '3b76b992-4a29-4220-b881-7f89c8ab8a89', 5);

INSERT INTO companies(name, address) VALUES('Microsoft', 'Thames Valley Park, Reading, Berkshire, RG6 1WG');
INSERT INTO companies(name, address) VALUES('Apple', 'Piazza Principessa Clotilde n. 8, 20121 Milano Italia. Netherlands');
INSERT INTO companies(name, address) VALUES('Coca-cola', 'Pemberton House Bakers Road Uxbridge, Middx UB8 1EZ United Kingdom');
INSERT INTO companies(name, address) VALUES('Nestle', 'avenue Nestlé 55, 1800 Vevey, Switzerland');
INSERT INTO companies(name, address) VALUES('Mercedes-Benz', 'Av. des Communautés 7, 1200 Bruxelles, Belgium');


INSERT INTO companies_employers(company_id, employer_id) VALUES(1, 1);
INSERT INTO companies_employers(company_id, employer_id) VALUES(2, 2);
INSERT INTO companies_employers(company_id, employer_id) VALUES(3, 3);
INSERT INTO companies_employers(company_id, employer_id) VALUES(4, 4);
INSERT INTO companies_employers(company_id, employer_id) VALUES(5, 5);
