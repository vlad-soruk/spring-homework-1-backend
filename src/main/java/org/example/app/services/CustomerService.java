package org.example.app.services;

import org.example.app.dao.CustomerDao;
import org.example.app.entities.Customer;
import org.example.app.repositories.AccountRepository;
import org.example.app.repositories.CompanyRepository;
import org.example.app.repositories.CustomerRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final AccountRepository accountRepository;
    private final CompanyRepository companyRepository;
    private final CustomerDao customerDao;

    public CustomerService(CustomerRepository customerRepository, AccountRepository accountRepository, CompanyRepository companyRepository) {
        this.customerRepository = customerRepository;
        this.accountRepository = accountRepository;
        this.companyRepository = companyRepository;
        customerDao = new CustomerDao(accountRepository, customerRepository, companyRepository);
    }

    public Customer save(Customer obj) {
        return customerDao.save(obj);
    }

    public boolean delete(Customer obj) {
        return customerDao.delete(obj);
    }

    public void deleteAll(List<Customer> entities) {
        customerDao.deleteAll(entities);
    }

    public void saveAll(List<Customer> entities) {
        customerDao.saveAll(entities);
    }

    public List<Customer> findAll() {
        return customerDao.findAll();
    }

    public boolean deleteById(long id) {
        return customerDao.deleteById(id);
    }

    public Customer getOne(long id) {
        return customerDao.getOne(id);
    }

    public void modifyCustomer(long id, String name, String email, Integer age) {
        customerDao.modifyCustomer(id, name, email, age);
    }

    public Optional<Customer> findByNameAndEmailAndAge(String name, String email, Integer age) {
        return customerDao.findByNameAndEmailAndAge(name, email, age);
    }
}
