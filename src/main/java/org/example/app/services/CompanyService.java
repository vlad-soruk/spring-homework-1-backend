package org.example.app.services;

import org.example.app.dao.CompanyDao;
import org.example.app.entities.Company;
import org.example.app.repositories.CompanyRepository;
import org.example.app.repositories.CustomerRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final CustomerRepository customerRepository;
    private final CompanyDao companyDao;


    public CompanyService(CompanyRepository companyRepository, CustomerRepository customerRepository) {
        this.companyRepository = companyRepository;
        this.customerRepository = customerRepository;
        this.companyDao = new CompanyDao(companyRepository, customerRepository);
    }

    public Company save(Company obj) {
        return companyDao.save(obj);
    }

    public boolean delete(Company obj) {
        return companyDao.delete(obj);
    }

    public void deleteAll(List<Company> entities) {
        companyDao.deleteAll(entities);
    }

    public void saveAll(List<Company> entities) {
        companyDao.saveAll(entities);
    }

    public List<Company> findAll() {
        return companyDao.findAll();
    }

    public boolean deleteById(long id) {
        return companyDao.deleteById(id);
    }

    public Company getOne(long id) {
        return companyDao.getOne(id);
    }

    public boolean addEmployerToCompany(long id, String name, String address) {
        return companyDao.addEmployerToCompany(id, name, address);
    }

    public void createCompany(String name, String address) {
        companyDao.createCompany(name, address);
    }
    public boolean deleteEmployer(long id, String name) {
        return companyDao.deleteEmployer(id, name);
    }
    public boolean deleteCompany(String name) {
        return companyDao.deleteCompany(name);
    }
}
