package org.example.app.services;

import org.example.app.utils.Currency;
import org.example.app.dao.AccountDao;
import org.example.app.entities.Account;
import org.example.app.repositories.AccountRepository;
import org.example.app.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccountService {
    private final AccountDao accountDao;
    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository, CustomerRepository customerRepository) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
        accountDao = new AccountDao(accountRepository, customerRepository);
    }
    public Account save(Account obj) {
        return accountDao.save(obj);
    }

    public boolean delete(Account obj) {
        return accountDao.delete(obj);
    }

    public void deleteAll(List<Account> entities) {
        accountDao.deleteAll(entities);
    }

    public void saveAll(List<Account> entities) {
        accountDao.saveAll(entities);
    }

    public List<Account> findAll() {
        return accountDao.findAll();
    }
    public Account findByNumber(String number) {
        return accountDao.findByNumber(number);
    }

    public boolean deleteById(long id) {
        return accountDao.deleteById(id);
    }

    public Account getOne(long id) {
        return accountDao.getOne(id);
    }
    public boolean replenishAccount(String number, double amount) {
        return accountDao.replenishAccount(number, amount);
    }
    public boolean withdrawMoney(String number, double amount) {
        return accountDao.withdrawMoney(number, amount);
    }

    public boolean transferMoney(String from, String to, double amount) {
        return accountDao.transferMoney(from, to, amount);
    }

    public boolean createNewAccount(long id, Currency currency) {
        return accountDao.createNewAccount(id, currency);
    }

    public boolean deleteAccount(long id, String number) {
        return accountDao.deleteAccount(id, number);
    }



    public void deleteByNumber(String number) {
        accountDao.deleteByNumber(number);
    }
}
