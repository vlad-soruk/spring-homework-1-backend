package org.example.app.controllers;

import org.example.app.services.CustomerService;
import org.example.app.entities.Customer;
import org.example.app.repositories.AccountRepository;
import org.example.app.repositories.CompanyRepository;
import org.example.app.repositories.CustomerRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    private final CustomerRepository customerRepository;
    private final AccountRepository accountRepository;
    private final CompanyRepository companyRepository;
    private final CustomerService customerService;

    public CustomerController(CustomerRepository customerRepository, AccountRepository accountRepository, CompanyRepository companyRepository) {
        this.customerRepository = customerRepository;
        this.accountRepository = accountRepository;
        this.companyRepository = companyRepository;
        this.customerService = new CustomerService(customerRepository, accountRepository, companyRepository);
    }

    @GetMapping()
    public List<Customer> getAllCustomers(){
        return customerService.findAll();
    }
    @GetMapping("/{customerId}")
    public Customer getCustomerInfo(@PathVariable("customerId") long id){
        return customerService.getOne(id);
    }
    @PostMapping("/create")
    public ResponseEntity<?> createCustomer(@RequestBody Customer customer){
        // JSON з тіла HTTP-запиту
        // (н-д, {"name": "Valya Tasam", "email": "valya_tasam@ukr.net", "age": 56})
        // завдяки бібліотеці jackson перетворюється
        // на об'єкт Customer (відповідно до структури цього класу) та передається
        // в метод контролера як параметр
        if(customer.getName() == null ||
                customer.getEmail() == null ||
                customer.getAge() == null) {
            return ResponseEntity.status(400).body("Name, email and age of a customer cannot be empty!");
        }
        else {
            Optional<Customer> maybeCustomer = customerService.findByNameAndEmailAndAge(customer.getName(), customer.getEmail(), customer.getAge());
            if(maybeCustomer.isPresent()) {
                return ResponseEntity.status(400).body("Customer with the same data already exists!");
            }
            else {
                customerService.save(customer);
                return ResponseEntity.status(201).body(customer);
            }
        }
    }

    @PutMapping("/modify/{customerId}")
    public ResponseEntity<?> modifyCustomer(@PathVariable("customerId") long id, @RequestBody Customer customer) {
        if (customerService.getOne(id) == null) {
            return ResponseEntity.status(400).body("Customer with id " + id + " doesn`t exist!");
        }
        customerService.modifyCustomer(
                id,
                customer.getName(),
                customer.getEmail(),
                customer.getAge() );
        return ResponseEntity.ok(customerService.getOne(id));
    }

    @DeleteMapping("/delete/{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("customerId") long id){
        if (customerService.getOne(id) == null) {
            return ResponseEntity.status(400).body("Customer with id " + id + " doesn`t exist!");
        }
        else {
            customerService.deleteById(id);
            return ResponseEntity.ok("Customer with id " + id + " successfully deleted!");
        }
    }
}
