package org.example.app.controllers;

import org.example.app.services.AccountService;
import org.example.app.utils.ReplenishOrWithdrawRequest;
import org.example.app.utils.TransferRequest;
import org.example.app.entities.Account;
import org.example.app.repositories.AccountRepository;
import org.example.app.repositories.CustomerRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;
    private final AccountService accountService;

    public AccountController(AccountRepository accountRepository, CustomerRepository customerRepository) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
        this.accountService = new AccountService(accountRepository, customerRepository);
    }

    @GetMapping()
    public List<Account> getAllAccounts() {
        return accountService.findAll();
    }

    @PutMapping("/replenish-account")
    public ResponseEntity<?> replenishAccount(@RequestBody ReplenishOrWithdrawRequest replenishRequest) {
        if(replenishRequest.getNumber()==null || replenishRequest.getAmount()<=0.0) {
            return ResponseEntity.status(400).body("Something went wrong! Number of account is absent or amount is incorrect (it should be more than zero)!");
        }
        else if( accountService.replenishAccount(replenishRequest.getNumber(), replenishRequest.getAmount()) ) {
            return ResponseEntity.status(200).body("You replenished account № '" + replenishRequest.getNumber() + "' by " + replenishRequest.getAmount());
        }
        else {
            return ResponseEntity.status(400).body("Something went wrong! Maybe number of account is incorrect!");
        }
    }

    @PutMapping("/withdraw-money")
    public ResponseEntity<?> withdrawMoney(@RequestBody ReplenishOrWithdrawRequest withdrawRequest) {
        if(withdrawRequest.getNumber()==null || withdrawRequest.getAmount()<=0.0) {
            return ResponseEntity.status(400).body("Something went wrong! Number of account is absent or amount is incorrect (it should be more than zero)!");
        }
        else if( accountService.withdrawMoney(withdrawRequest.getNumber(), withdrawRequest.getAmount()) ) {
            return ResponseEntity.status(200).body("You withdrew " + withdrawRequest.getAmount() + " from account № '" + withdrawRequest.getNumber() + "'");
        }
        else {
            return ResponseEntity.status(400).body("Something went wrong! Probably, account has no sufficient amount of money to withdraw '" + withdrawRequest.getAmount() + "'" + " or number of account is wrong!");
        }
    }

    @PostMapping("/transfer")
    public ResponseEntity<?> transferMoney(@RequestBody TransferRequest transferRequest) {
        if(transferRequest.getFrom()==null || transferRequest.getTo()==null || transferRequest.getAmount()<=0.0) {
            return ResponseEntity.status(400).body("Something went wrong! Number of account (from or to) is absent or amount is incorrect (it should be more than zero)!");
        }
        else if( accountService.transferMoney(transferRequest.getFrom(), transferRequest.getTo(), transferRequest.getAmount()) ) {
            return ResponseEntity.status(200).body("You transferred " + transferRequest.getAmount() + " from account № '" + transferRequest.getFrom() + "'" + " to account № '" + transferRequest.getTo() + "'");
        }
        else {
            return ResponseEntity.status(400).body("Something went wrong! Probably, account has no sufficient amount of money to transfer '" + transferRequest.getAmount() + "'" + " or some number of account is wrong!");
        }
    }

    @PostMapping("/create-account/{customerId}")
    public ResponseEntity<?> createNewAccount(@PathVariable("customerId") long id, @RequestBody Account acc) {
        if(acc.getCurrency() == null) {
            return ResponseEntity.status(400).body("You should indicate currency of customer`s account!");
        }
        else if (accountService.createNewAccount(id, acc.getCurrency())) {
            return ResponseEntity.status(201).body("New account was successfully added to user with id '" + id + "'");
        }
        else {
            return ResponseEntity.status(400).body("Something went wrong! Maybe customer with id " + id + " doesn`t exist!");
        }
    }

    @DeleteMapping("/delete-account")
    public ResponseEntity<?> deleteAccount(@RequestParam("customerId") Long id, @RequestParam("number") String number) {
        if(number == null) {
            return ResponseEntity.status(400).body("You should indicate number of the account you want to delete!");
        }
        else if( accountService.deleteAccount(id, number)) {
            return ResponseEntity.status(201).body("Account № '" + number + "' was successfully removed from user with id '" + id + "'");
        }
        else {
            return ResponseEntity.status(400).body("Something went wrong! Maybe customer doesn`t have account with number '" + number + "'");
        }
    }



}
