package org.example.app.controllers;

import org.example.app.services.CompanyService;
import org.example.app.entities.Company;
import org.example.app.repositories.CompanyRepository;
import org.example.app.repositories.CustomerRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("companies")
public class CompanyController {
    private final CompanyRepository companyRepository;
    private final CustomerRepository customerRepository;
    private final CompanyService companyService;
    public CompanyController(CompanyRepository companyRepository, CustomerRepository customerRepository) {
        this.companyRepository = companyRepository;
        this.customerRepository = customerRepository;
        this.companyService = new CompanyService(companyRepository, customerRepository);
    }

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyService.findAll();
    }

    @GetMapping("{companyId}")
    public Company getOneCompany(@PathVariable("companyId") long id) {
        return companyService.getOne(id);
    }

    @PostMapping("/add-employer/{customerId}")
    public ResponseEntity<String> addEmployerToCompany(@PathVariable("customerId") long id, @RequestBody Company company) {
        if(company.getName() == null || company.getAddress() == null) {
            return ResponseEntity.status(400).body("You should indicate name and address of the company!");
        }
        else if(companyRepository.findByNameAndAddress(company.getName(), company.getAddress()).isEmpty()) {
            return ResponseEntity.status(400).body("Company with name '" + company.getName() + "' and address + '" + company.getAddress() + "' doesn`t exist in database!");
        }
        else if( companyService.addEmployerToCompany(id, company.getName(), company.getAddress()) ) {
            return ResponseEntity.status(201).body("New employer was successfully added to '" + company.getName() + "'");
        }
        else {
            return ResponseEntity.status(400).body("Something went wrong! Probably, employer with id '" + id + "' is already working at '" + company.getName() + "'");
        }
    }

    @PostMapping("/create-company")
    public ResponseEntity<String> createCompany(@RequestBody Company company) {
        if(company.getName() == null || company.getAddress() == null) {
            return ResponseEntity.status(400).body("You should indicate name and address of the company!");
        }
        else if(companyRepository.findByNameAndAddress(company.getName(), company.getAddress()).isPresent()) {
            return ResponseEntity.status(200).body("Company with name '" + company.getName() + "' and address + '" + company.getAddress() + "' already exists!");
        }
        companyService.createCompany(company.getName(), company.getAddress());
        return ResponseEntity.status(201).body("New company '" + company.getName() + "' was successfully added to the database!");
    }

    @DeleteMapping("/delete-employer")
    public ResponseEntity<?> deleteEmployer(@RequestParam("employerId") Long id, @RequestParam("compName") String name) {
        if(id == null || name == null) {
            return ResponseEntity.status(400).body("You should indicate the name of the company you want to delete and employer`s id!");
        }
        else if(companyService.deleteEmployer(id, name)) {
            return ResponseEntity.status(201).body("Company '" + name + "' was successfully removed from user with id '" + id + "'");
        }
        else {
            return ResponseEntity.status(400).body("Something went wrong! Maybe customer doesn`t work in company with name '" + name + "'");
        }
    }
    @DeleteMapping("/delete-company")
    public ResponseEntity<?> deleteCompany(@RequestParam("name") String name) {
        if(name == null) {
            return ResponseEntity.status(400).body("You should indicate the name of the company you want to delete!");
        }
        else if(companyService.deleteCompany(name)) {
            return ResponseEntity.status(201).body("Company '" + name + "' was successfully removed from database");
        }
        else {
            return ResponseEntity.status(400).body("Something went wrong! Maybe company '" + name + "' is already deleted or doesn`t exist in database!");
        }
    }
}
