package org.example.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.example.app.utils.Currency;

import java.util.UUID;

@Entity
@Table(name = "accounts")
@Data
@ToString(exclude = {"customer"})
public class Account extends AbstractEntity {
    @Column
    private String number = UUID.randomUUID().toString();

    @Column
    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Column
    private Double balance = 0d;

    @JsonIgnore
    @EqualsAndHashCode.Exclude()
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public Account() {}

    public Account(Currency currency, Customer customer) {
        this.currency = currency;
        this.customer = customer;
    }

    public void replenishAccount(double amount) {
        this.balance += amount;
    }
    public void withdrawMoney(double amount) {
        this.balance -= amount;
    }

//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder("Account{");
////        sb.append("id=").append(id);
//        sb.append("number='").append(number).append('\'');
//        sb.append(", currency=").append(currency);
//        sb.append(", balance=").append(balance);
////        sb.append(", customer=").append(customer);
//        sb.append('}');
//        return sb.toString();
//    }

//    public String showAccountInfo() {
//        final StringBuilder sb = new StringBuilder("Account{");
//        sb.append("id=").append(id);
//        sb.append("number='").append(number).append('\'');
//        sb.append(", currency=").append(currency);
//        sb.append(", balance=").append(balance);
//        sb.append(", customer=").append(customer);
//        sb.append('}');
//        return sb.toString();
//    }
}
