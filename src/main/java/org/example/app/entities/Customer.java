package org.example.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.Set;

@Entity
@Table(name = "customers")
@Data
@ToString(exclude = "companies")
@NoArgsConstructor
@AllArgsConstructor
public class Customer extends AbstractEntity {
    @Column
    private String name;

    @Column
    private String email;

    @Column
    private Integer age;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.REMOVE)
    private List<Account> accounts;

    @JsonIgnore
    @EqualsAndHashCode.Exclude()
    @ManyToMany(mappedBy = "employers", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
//    @JsonBackReference
    private Set<Company> companies;

    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }
}
