package org.example.app.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Table(name = "companies")
@Data
@ToString(exclude = "employers")
@NoArgsConstructor
public class Company extends AbstractEntity {
    @Column
    private String name;

    @Column
    private String address;

//    @JsonIgnore
//    @JoinTable
    @EqualsAndHashCode.Exclude()
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "companies_employers",
            joinColumns = {@JoinColumn(name = "company_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "employer_id", referencedColumnName = "id")})
//    @JsonManagedReference
    private Set<Customer> employers;

    public Company(String name, String address) {
        this.name = name;
        this.address = address;
//        this.employers = employers;
    }
    public Company(String name, String address, Set<Customer> employers) {
        this.name = name;
        this.address = address;
        this.employers = employers;
    }
}
