package org.example.app.dao;

import org.example.app.entities.Company;
import org.example.app.entities.Customer;
import org.example.app.repositories.CompanyRepository;
import org.example.app.repositories.CustomerRepository;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public class CompanyDao implements Dao<Company>{
    private final CompanyRepository companyRepository;
    private final CustomerRepository customerRepository;
    public CompanyDao(CompanyRepository companyRepository, CustomerRepository customerRepository) {
        this.companyRepository = companyRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public Company save(Company obj) {
        return companyRepository.save(obj);
    }

    @Override
    public boolean delete(Company obj) {
        Optional<Company> optionalCompany = companyRepository.findById(obj.getId());
        if(optionalCompany.isPresent()) {
            companyRepository.delete(obj);
            return true;
        }
        return false;
    }

    @Override
    public void deleteAll(List<Company> entities) {
        companyRepository.deleteAll(entities);
    }

    @Override
    public void saveAll(List<Company> entities) {
        companyRepository.saveAll(entities);
    }

    @Override
    public List<Company> findAll() {
        return companyRepository.findAll(Sort.by(Sort.Order.asc("id")));
    }

    @Override
    public boolean deleteById(long id) {
        Optional<Company> optionalComp = companyRepository.findById(id);
        if(optionalComp.isPresent()) {
            companyRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public Company getOne(long id) {
        Optional<Company> optionalCompany = companyRepository.findById(id);
        return optionalCompany.orElse(null);
    }

    public boolean addEmployerToCompany(long id, String name, String address) {
        Optional<Customer> maybeCustomer = customerRepository.findById(id);
        Optional<Company> maybeCompany = companyRepository.findByNameAndAddress(name, address);

        if (maybeCustomer.isPresent() && maybeCompany.isPresent()) {
            if ( maybeCompany.get().getEmployers().contains(maybeCustomer.get()) ) {
                return false;
            }
            else {
                companyRepository.addEmployerToCompany(maybeCompany.get().getId(), id);
                return true;
            }
        }
        else {
            return false;
        }
    }

    public void createCompany(String name, String address) {
        companyRepository.save(new Company(name, address));
    }

    public boolean deleteEmployer(long id, String name) {
        Optional<Customer> maybeCustomer = customerRepository.findById(id);
        Optional<Company> maybeCompany = companyRepository.findByName(name);

        if(maybeCustomer.isPresent() && maybeCompany.isPresent()) {
            companyRepository.deleteEmployer(maybeCompany.get().getId(), maybeCustomer.get().getId());
            return true;
        }
        return false;
    }
    public boolean deleteCompany(String name) {
        Optional<Company> maybeCompany = companyRepository.findByName(name);

        if(maybeCompany.isPresent()) {
            companyRepository.delete(maybeCompany.get());
            return true;
        }
        return false;
    }
}

