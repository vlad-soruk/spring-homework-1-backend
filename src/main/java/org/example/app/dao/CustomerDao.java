package org.example.app.dao;

import org.example.app.entities.Company;
import org.example.app.entities.Customer;
import org.example.app.repositories.AccountRepository;
import org.example.app.repositories.CompanyRepository;
import org.example.app.repositories.CustomerRepository;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class CustomerDao implements Dao<Customer>{
    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;
    private final CompanyRepository companyRepository;
    public CustomerDao(AccountRepository accountRepository, CustomerRepository customerRepository, CompanyRepository companyRepository) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
        this.companyRepository = companyRepository;
    }

    @Override
    public Customer save(Customer obj) {
        return customerRepository.save(obj);
    }

    @Override
    public boolean delete(Customer obj) {
        Optional<Customer> optionalCustomer = customerRepository.findById(obj.getId());
        if(optionalCustomer.isPresent()) {
            customerRepository.delete(obj);
            return true;
        }
        return false;
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        customerRepository.deleteAll(entities);
    }

    @Override
    public void saveAll(List<Customer> entities) {
        customerRepository.saveAll(entities);
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll(Sort.by(Sort.Order.asc("id")));
    }

    @Override
    public boolean deleteById(long id) {
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if(optionalCustomer.isPresent()) {
            Set<Company> companies = optionalCustomer.get().getCompanies();

            if(!companies.isEmpty()) {
                for(Company company : companies) {
                    companyRepository.deleteEmployer(company.getId(), optionalCustomer.get().getId());
                }
            }
            customerRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public Customer getOne(long id) {
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        optionalCustomer.ifPresent(customer -> System.out.println(customer.getCompanies()));

        return optionalCustomer.orElse(null);
    }

    public void modifyCustomer(long id, String name, String email, Integer age) {
        customerRepository.modifyCustomer(id, name, email, age);
    }

    public Optional<Customer> findByNameAndEmailAndAge(String name, String email, Integer age) {
        return customerRepository.findByNameAndEmailAndAge(name, email, age);
    }
}
