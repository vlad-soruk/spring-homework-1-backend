package org.example.app.dao;

import org.example.app.entities.Account;
import org.example.app.entities.Customer;
import org.example.app.repositories.AccountRepository;
import org.example.app.repositories.CustomerRepository;
import org.example.app.utils.Currency;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public class AccountDao implements Dao<Account> {
    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;
    public AccountDao(AccountRepository accountRepository, CustomerRepository customerRepository) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public Account save(Account obj) {
        return accountRepository.save(obj);
    }

    @Override
    public boolean delete(Account obj) {
        Optional<Account> optionalAccount = accountRepository.findById(obj.getId());
        if(optionalAccount.isPresent()) {
            accountRepository.delete(obj);
            return true;
        }
        return false;
    }

    @Override
    public void deleteAll(List<Account> entities) {
        accountRepository.deleteAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        accountRepository.saveAll(entities);
    }

    @Override
    public List<Account> findAll() {
        return accountRepository.findAll(Sort.by(Sort.Order.asc("id")));
    }

    public Account findByNumber(String number) {
        Optional<Account> optionalAccount = accountRepository.findByNumber(number);
        return optionalAccount.orElse(null);
    }

    @Override
    public boolean deleteById(long id) {
        Optional<Account> optionalAcc = accountRepository.findById(id);
        if(optionalAcc.isPresent()) {
            accountRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public Account getOne(long id) {
        Optional<Account> optionalAccount = accountRepository.findById(id);
        return optionalAccount.orElse(null);
    }

    public boolean replenishAccount(String number, double amount) {
        return accountRepository.replenishAccount(number, amount) == 1;
    }

    public boolean withdrawMoney(String number, double amount) {
        return accountRepository.withdrawMoney(number, amount) == 1;
    }

    public boolean transferMoney(String from, String to, double amount) {
        return accountRepository.transferMoney(from, to, amount);
    }

    public boolean createNewAccount(long id, Currency currency) {
        Optional<Customer> maybeCustomer = customerRepository.findById(id);
        if (maybeCustomer.isPresent()) {
            accountRepository.save(new Account(currency, customerRepository.findById(id).get()));
            return true;
        }
        else {
            return false;
        }
    }

    public boolean deleteAccount(long id, String number) {
        Optional<Account> maybeAccount = accountRepository.findByNumber(number);
        Optional<Customer> maybeCustomer = customerRepository.findById(id);

        if( maybeAccount.isPresent() && maybeCustomer.isPresent() ) {
            Optional<Account> maybeCustomerAccount = maybeCustomer.get().getAccounts().stream().filter(a -> a.getNumber().equals(number)).findAny();
            if(maybeCustomerAccount.isPresent()) {
                accountRepository.delete(maybeAccount.get());
                return true;
            }
        }
        return false;
    }

    public void deleteByNumber(String number) {
        accountRepository.deleteByNumber(number);
    }

}
