package org.example.app.repositories;

import jakarta.transaction.Transactional;
import org.example.app.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    @Transactional
    void deleteByNumber(String number);
    Optional<Account> findByNumber(String number);

    @Modifying
    @Transactional
    @Query("UPDATE Account a SET a.balance = a.balance + CAST(:amount AS java.lang.Double) WHERE a.number = :number")
    int replenishAccount(@Param("number") String number, @Param("amount") double amount);

    @Modifying
    @Transactional
    @Query("UPDATE Account a SET a.balance = a.balance - CAST(:amount AS java.lang.Double) WHERE a.number = :number AND a.balance >= :amount")
    int withdrawMoney(@Param("number") String number, @Param("amount") double amount);

    default boolean transferMoney(String from, String to, double amount) {
        Optional<Account> optionalAccountFrom = findByNumber(from);
        Optional<Account> optionalAccountTo = findByNumber(to);
        boolean withdrawSuccess = withdrawMoney(from, amount) == 1;

        if(optionalAccountFrom.isPresent() && optionalAccountTo.isPresent() && withdrawSuccess) {
            replenishAccount(to, amount);
            return true;
        }

        return false;
    }

//    @Modifying
//    @Query(value = "INSERT INTO accounts(balance, currency, number, customer_id) VALUES(0, :currency, :number, :id)", nativeQuery = true)
//    @Transactional
//    void createNewAccount(@Param("currency") String currency, @Param("number") String number, @Param("id") long id);

}
