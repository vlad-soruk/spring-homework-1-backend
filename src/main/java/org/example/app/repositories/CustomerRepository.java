package org.example.app.repositories;

import jakarta.transaction.Transactional;
import org.example.app.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByNameAndEmailAndAge(String name, String email, int age);

    @Modifying
    @Transactional
    @Query("""
            UPDATE Customer c
            SET c.name = COALESCE(:name, c.name),
            c.email = COALESCE(:email, c.email),
            c.age = COALESCE(:age, c.age)
            WHERE c.id = :id
           """)
    int modifyCustomer(@Param("id") Long id, @Param("name") String name, @Param("email") String email, @Param("age") Integer age);
}
