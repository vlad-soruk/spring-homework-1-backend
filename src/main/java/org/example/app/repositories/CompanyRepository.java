package org.example.app.repositories;

import jakarta.transaction.Transactional;
import org.example.app.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
//    @Query(value = "SELECT id, name, address, employer_id FROM companies WHERE name = :name AND employer_id = :emp_id", nativeQuery = true)
//    Optional<Company> findByNameAndEmployerId(@Param("name") String name, @Param("emp_id") long id);

    Optional<Company> findByNameAndAddress(String name, String address);
    Optional<Company> findByName(String name);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO companies_employers(company_id, employer_id) VALUES(:comp_id, :emp_id)", nativeQuery = true)
    void addEmployerToCompany(@Param("comp_id") Long companyId, @Param("emp_id") Long customerId);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM companies_employers WHERE company_id = :comp_id AND employer_id = :emp_id", nativeQuery = true)
    void deleteEmployer(@Param("comp_id") Long companyId, @Param("emp_id") Long customerId);
}
