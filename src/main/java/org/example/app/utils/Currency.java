package org.example.app.utils;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
