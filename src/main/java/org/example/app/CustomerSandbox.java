//package org.example.app;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.Data;
//import lombok.RequiredArgsConstructor;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@RequiredArgsConstructor
//@Data
//public class CustomerSandbox {
//    private Long id;
//    private final String name;
//    private final String email;
//    private final Integer age;
//
//    @JsonIgnore
//    private List<Account> accounts = new ArrayList<>();
//    public void addAccount(Account acc) {
//        accounts.add(acc);
//    }
//
//    public static void main(String[] args) throws JsonProcessingException {
//        CustomerSandbox customerSandbox = new CustomerSandbox("Vlad", "Rka", 18);
//        System.out.println(customerSandbox);
//        customerSandbox.addAccount(new Account(Currency.EUR, new Customer("Vlad", "Rka", 18)));
//        System.out.println(customerSandbox);
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        String s = objectMapper.writeValueAsString(customerSandbox);
//        System.out.printf("JSON: %s", s);
//
//        Customer customer = objectMapper.readValue(s, Customer.class);
//        System.out.println();
//        System.out.println(customer);
//
//        ReplenishOrWithdrawRequest efwefwef = new ReplenishOrWithdrawRequest();
//        System.out.println(efwefwef);
//    }
//}
