//package org.example.app;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.Data;
//import lombok.RequiredArgsConstructor;
//
//import java.util.UUID;
//
//@RequiredArgsConstructor
//@Data
//public class AccountSandbox {
//    private Long id;
//    private String number = UUID.randomUUID().toString();
//    private final Currency currency;
//    private Double balance;
//    private final Customer customer;
//
//    public static void main(String[] args) throws JsonProcessingException {
//        AccountSandbox account1 = new AccountSandbox(Currency.EUR, null);
//        System.out.println(account1);
//        AccountSandbox account2 = new AccountSandbox(Currency.USD, null);
//        System.out.println(account2);
//        AccountSandbox account3 = new AccountSandbox(Currency.USD, null);
//        System.out.println(account3);
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        String s = objectMapper.writeValueAsString(account1);
//        System.out.println(s);
//
//        Account acc1readed = objectMapper.readValue(s, Account.class);
//        System.out.println(acc1readed);
//    }
//}
