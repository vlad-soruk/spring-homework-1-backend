package org.example.app;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // In order to avoid problem of fetching json from browser
        // (Access to fetch at 'http://localhost:9000/customers' from origin
        // 'http://localhost:3000' has been blocked by CORS policy:
        // No 'Access-Control-Allow-Origin' header is present on the requested resource)
        // we should configure our Spring App
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:3000")
                .allowedMethods("GET", "POST", "PUT", "DELETE")
                .allowedHeaders("*");
    }
}